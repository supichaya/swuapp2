var gulp = require('gulp');

var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-ruby-sass');


var libJSFile = [
    './assets/jquery/dist/jquery.min.js',
    './assets/bootstrap4/dist/js/bootstrap.min.js',
    
];

var libCSSFile = [
    './assets/bootstrap4/dist/css/bootstrap.min.css', 
];



gulp.task ('css',function(){
   // return gulp
    sass('assets/mylib/css/style.scss',{
        style:'compressed'
    })  
    .on('error',sass.logError)
    //.pipe(minifyCSS())
    .pipe(rename({suffix : '.min'}))
    .pipe(gulp.dest('./wwwroot/css'));
});

gulp.task ('js',function(){
    return gulp
    .src('assets/mylib/js/script.js')    
    .pipe(minifyJS())
    .pipe(rename({suffix : '.min'}))
    .pipe(gulp.dest('./wwwroot/js'));
});

gulp.task ('libjs',function(){
    return gulp
    .src(libJSFile)    
    .pipe(gulp.dest('./wwwroot/js'));
});

gulp.task ('libcss',function(){
    return gulp
    .src(libCSSFile)    
    .pipe(gulp.dest('./wwwroot/css'));
});


gulp.task ('all',['css','js','libjs','libcss']);
